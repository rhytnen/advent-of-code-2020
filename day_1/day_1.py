def find_sum_pair(numbers, target=2020):
    for idx, num in enumerate(numbers[:-1]):
        if target - num in numbers[idx:]:
            return num, target - num


def day_1_1(numbers):
    x, y = find_sum_pair(numbers)
    return x * y


def day_1_2(numbers):
    for num in numbers:
        pair = find_sum_pair(numbers, 2020 - num)
        if pair:
            return num * pair[0] * pair[1]


def main():
    numbers = sorted([int(num) for num in open("day_1.txt", "r").readlines()])
    print("Day 1")
    print(f"  Puzzle 1: {day_1_1(numbers)}")
    print(f"  Puzzle 2: {day_1_1(numbers)}")


if __name__ == "__main__":
    main()
