from functools import partial
import re


def re_validator(pattern, value):
    pat = re.compile(pattern)
    if not pat.match(value):
        raise ValueError


def int_range_validator(lo, hi, value):
    if not lo <= int(value) <= hi:
        raise ValueError
    return True


def hgt_validator(height):
    unit = height[-2:].lower()
    value = int(height[:-2])
    if unit == "cm":
        int_range_validator(150, 193, value)
    elif unit == "in":
        int_range_validator(59, 76, value)
    else:
        raise ValueError


def content_validator(content, value):
    if value not in content:
        raise ValueError


def validate_passport(data, validate_content=False):
    required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    validators = {
        "byr": partial(int_range_validator, 1920, 2002),
        "iyr": partial(int_range_validator, 2010, 2020),
        "eyr": partial(int_range_validator, 2020, 2030),
        "hgt": hgt_validator,
        "hcl": partial(re_validator, "^#[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f]$"),
        "ecl": partial(content_validator, ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]),
        "pid": partial(re_validator, "^\d\d\d\d\d\d\d\d\d$"),
        "cid": lambda x: True,  # could just catch a KeyError in the validation loop and ignore them
    }

    data = data.replace(" ", "\n").split("\n")
    passport = dict(
        zip(
            [field.split(":")[0] for field in data],
            [field.split(":")[1] for field in data],
        )
    )

    for req in required:
        if req not in passport.keys():
            return False

    if validate_content:
        for k, v in passport.items():
            try:
                validators[k](v)
            except (IndexError, ValueError, TypeError):
                return False
    return True


def day_4_1(passports):
    return sum(map(validate_passport, passports))


def day_4_2(passports):
    return sum(map(validate_passport, passports, [True] * len(passports)))


def main():
    passports = open("day_4.txt", "r").read().split("\n\n")
    print("Day 4")
    print(f"  Puzzle 1: {day_4_1(passports)}")
    print(f"  Puzzle 2: {day_4_2(passports)}")


if __name__ == "__main__":
    main()
