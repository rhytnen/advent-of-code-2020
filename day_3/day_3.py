import math


def count_trees_on_slope(tree_map, dx, dy):
    tree = "#"
    x = 0
    y = 0
    num_trees = 0
    width = len(tree_map[0])
    height = len(tree_map)

    while y < height - 1:
        x += dx
        y += dy
        if x >= width:
            x -= width
        if tree_map[y][x] == tree:
            num_trees += 1

    return num_trees


def day_3_1(tree_map):
    return count_trees_on_slope(tree_map, 3, 1)


def day_3_2(tree_map):
    slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
    return math.prod([count_trees_on_slope(tree_map, s[0], s[1]) for s in slopes])


def main():
    tree_map = [line.strip() for line in open("day_3.txt", "r").readlines()]
    print("Day 3")
    print(f"  Puzzle 1: {day_3_1(tree_map)}")
    print(f"  Puzzle 2: {day_3_2(tree_map)}")


if __name__ == "__main__":
    main()
