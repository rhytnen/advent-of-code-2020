def day_2_1(lines):
    valid = 0
    for rng, key, password in lines:
        lo, hi = rng.split("-")
        if int(lo) <= password.count(key[0]) <= int(hi):
            valid += 1
    return valid


def day_2_2(lines):
    valid = 0
    for rng, key, password in lines:
        lo, hi = rng.split("-")
        if (password[int(lo) - 1] + password[int(hi) - 1]).count(key[0]) == 1:
            valid += 1
    return valid


def main():
    lines = [line.strip().split(" ") for line in open("day_2.txt", "r").readlines()]
    print("Day 2")
    print(f"  Puzzle 1: {day_2_1(lines)}")
    print(f"  Puzzle 2: {day_2_2(lines)}")


if __name__ == "__main__":
    main()
