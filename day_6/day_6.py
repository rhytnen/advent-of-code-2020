def day_6_1(groups):
    return sum([len(set(group.replace('\n', ''))) for group in groups])


def day_6_2(groups):
    count = 0
    for group in groups:
        people = [set(a) for a in group.strip('\n').split('\n')]
        current = people[0]
        for person in people[1:]:
            current = current.intersection(person)
        count+= len(current)
    return count

def main():
    groups = open("day_6.txt", "r").read().split("\n\n")
    print("Day 6")
    print(f"  Puzzle 1: {day_6_1(groups)}")
    print(f"  Puzzle 2: {day_6_2(groups)}")


if __name__ == "__main__":
    main()