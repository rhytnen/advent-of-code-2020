import sys
import argparse
import os

def run_day_x_script(day):
    if os.path.exists(f'day_{day}'):
        os.chdir(f'day_{day}')
        os.system(f'python day_{day}.py')
        os.chdir('..')


def main():
    parser = argparse.ArgumentParser(description="""Runner script for puzzles""",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('day', default='all', help='Which day to run?  [All | 1, 2, etc]')
    args = parser.parse_args(sys.argv[1:])

    if args.day == 'all':
        for i in range(1,26):
            run_day_x_script(i)
    else:
        run_day_x_script(args.day)

if __name__ == '__main__':
    main()
