def day_5_1(seats):
    return max([int(seat, 2) for seat in seats])


def day_5_2(seats):
    ids = sorted([int(seat, 2) for seat in seats])
    return sum(range(ids[-1] + 1)) - sum(range(ids[0])) - sum(ids)


def main():
    seats = [
        line.strip().replace("F", "0").replace("L", "0").replace("R", "1").replace("B", "1")
        for line in open("day_5.txt", "r").readlines()
    ]
    print("Day 5")
    print(f"  Puzzle 1: {day_5_1(seats)}")
    print(f"  Puzzle 2: {day_5_2(seats)}")


if __name__ == "__main__":
    main()
